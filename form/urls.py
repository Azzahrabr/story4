from django.urls import path

from . import views
app_name = "form"
urlpatterns = [
    path('matkul/P<int:id_nama_matkul>/', views.matkuldetail, name='matkuldetail'),
    path('delete/P<int:delete_id>/', views.delete, name='delete'),
    path('viewpage/', views.viewpage, name='viewpage'),
    path('create/', views.create, name='create'),
    path('', views.index, name='index'),
]
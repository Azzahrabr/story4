from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect

# Create your views here.
from .forms import PostForm
from .models import PostModel

def index(request):
    posts = PostModel.objects.all()
    context = {
        'page_title':'List of Courses',
        'posts':posts,
    }

    return render(request,'form/index.html',context)

def create(request):
    post_form = PostForm()

    if request.method == 'POST':
        PostModel.objects.create(
            nama_matkul       = request.POST.get('nama_matkul'),
            jumlah_sks        = request.POST.get('jumlah_sks'),
            ruangan    = request.POST.get('ruangan'),
            dosen    = request.POST.get('dosen'),
            deskripsi    = request.POST.get('deskripsi'),
            semester_tahun    = request.POST.get('semester_tahun'),



        )
        return HttpResponseRedirect("/form/")

    context = {
        'page_title':'Add course',
        'post_form':post_form
    }

    return render(request,'form/create.html',context)

def viewpage(request):
    return render(request,'form/viewpage.html')

def delete(request, delete_id):
    try:
        jadwal_to_delete = PostModel.objects.get(id = delete_id)
        jadwal_to_delete.delete()
        return redirect('form:index')
    except:
        return redirect('form:index')

def matkuldetail(request, id_nama_matkul):
    nama_matkul = PostModel.objects.filter(id=id_nama_matkul).get()
    response = {
        'page_title':'Detail Course',
        'nama_matkul':nama_matkul
    }
    
    return render(request, 'form/matkuldetail.html',response)


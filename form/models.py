from django.db import models

# Create your models here.

class PostModel(models.Model):
    nama_matkul     = models.CharField(max_length = 50)
    jumlah_sks      = models.IntegerField()
    ruangan         = models.CharField(max_length = 100)
    dosen           = models.CharField(max_length = 100)
    deskripsi       = models.CharField(max_length = 1000, null = True)
    semester_tahun  = models.CharField(max_length = 100)

    published       = models.DateTimeField(auto_now_add = True)
    updated         = models.DateTimeField(auto_now = True)

    def __str__(self):
        return "{}.{}".format(self.id,self.nama_matkul)
from django.shortcuts import render


def home(request):
    return render(request, 'main/home.html')

def aboutme(request):
    return render(request, 'main/aboutme.html')

def experience(request):
    return render(request, 'main/experience.html')

def contact(request):
    return render(request, 'main/contact.html')

def gallery(request):
    return render(request, 'main/gallery.html')

def story1(request):
    return render(request, 'main/story1.html')


from django.urls import path

from . import views
app_name = "story6"
urlpatterns = [
    path('register/P<int:task_id>/', views.register, name='register'),
    path('delete/P<int:delete_id>/', views.delete, name='delete'),
    path('listKegiatan/', views.listKegiatan, name='listKegiatan'),
    path('tambahKegiatan/', views.tambahKegiatan, name='tambahKegiatan'),
    path('index/', views.index, name='index'),

    # path('', views.index, name='index'),
]
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect

# Create your views here.
from .forms import FormKegiatan , FormOrang
from .models import Kegiatan, DaftarOrang


def index(request):
    return render(request,'story6/index.html')


def tambahKegiatan(request):
    form_kegiatan = FormKegiatan()
    if request.method == 'POST':
        form_kegiatan_input = FormKegiatan(request.POST)
        if form_kegiatan_input.is_valid():
            Kegiatan.objects.create(
            nama_kegiatan= form_kegiatan_input.cleaned_data.get('nama_kegiatan'),
        )
        return HttpResponseRedirect("/story6/listKegiatan/")
    context = {
        'page_title_kegiatan':'Add Activity',
        'form_kegiatan':form_kegiatan
    }

    return render(request,'story6/tambahKegiatan.html',context)


def listKegiatan(request):
    kegiatan = Kegiatan.objects.all()
    orang = DaftarOrang.objects.all()
    context = {
        'page_title':'List of Activity',
        'kegiatan':kegiatan,
        'orang': orang,
    }

    return render(request,'story6/listKegiatan.html',context)



def delete(request, delete_id):
    try:
        orang_to_delete = DaftarOrang.objects.filter(id = delete_id)
        orang_to_delete.delete()
        return redirect('story6:listKegiatan')
    except:
        return redirect('story6:listKegiatan')


def register(request, task_id):
    form_orang = FormOrang()

    if request.method == 'POST':
        form_orang_input = FormOrang(request.POST)
        if form_orang_input.is_valid():
            data = form_orang_input.cleaned_data
            orangOrang = DaftarOrang()
            orangOrang.nama_orang = data['nama_orang']
            orangOrang.kegiatan = Kegiatan.objects.get(id=task_id)
            orangOrang.save()
            # Orang.objects.create(
            # nama_orang= form_orang_input.cleaned_data.get('nama_orang'),
            # nama_orang.kegiatan = Kegiatan.objects.get(id=task_id)
            # nama_orang.save()
        # )
        return HttpResponseRedirect("/story6/listKegiatan/")

    context = {
        'page_title':'Whats your name',
        'form_orang':form_orang
    }
    return render(request,'story6/register.html',context)






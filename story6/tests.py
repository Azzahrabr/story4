from django.test import TestCase
from django.test import Client 
from django.urls import resolve, reverse
from .models import Kegiatan, DaftarOrang
from .views import index, tambahKegiatan, listKegiatan, delete, register
from .forms import FormKegiatan , FormOrang
from .apps import Story6Config

# Create your tests here.

class TestModel(TestCase):
    def setUp(self):
        self.kegiatan = Kegiatan.objects.create(
            nama_kegiatan="memotong sayur", deskripsi="membantu mama"
        )
        self.orang = DaftarOrang.objects.create(
            nama_orang = "Azzahra"
        )

    def test_instance_created(self):
        self.assertEqual(Kegiatan.objects.count(), 1)
        self.assertEqual(DaftarOrang.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.kegiatan), "memotong sayur")
        self.assertEqual(str(self.orang), "Azzahra")


class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.kegiatan = Kegiatan.objects.create(
            nama_kegiatan="memotong sayur", deskripsi="membantu mama")
        self.orang = DaftarOrang.objects.create(
            nama_orang="Azzahra", kegiatan=Kegiatan.objects.get(nama_kegiatan="memotong sayur"))
        self.index = reverse("story6:index")
        self.tambahKegiatan = reverse("story6:tambahKegiatan")
        self.listKegiatan = reverse("story6:listKegiatan")
        self.register = reverse("story6:register", args=[self.kegiatan.pk])
        self.delete = reverse("story6:delete", args=[self.orang.pk])

    def test_GET_index(self):
        response = self.client.get(self.index)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story6/index.html')

    def test_GET_listKegiatan(self):
        response = self.client.get(self.listKegiatan)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story6/listKegiatan.html')

    def test_GET_tambahKegiatan(self):
        response = self.client.get(self.tambahKegiatan)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story6/tambahKegiatan.html')

    def test_POST_tambahKegiatan(self):
        response = self.client.post(self.tambahKegiatan,{'nama': 'liburan', 'deskripsi': "belajar"}, follow = True)
        self.assertEqual(response.status_code, 200)

    def test_POST_tambahKegiatan_invalid(self):
        response = self.client.post(self.tambahKegiatan,{'nama': '', 'deskripsi': ""}, follow = True)
        self.assertTemplateUsed(response, 'story6/listKegiatan.html')

    def test_GET_delete(self):
        kegiatan = Kegiatan(nama_kegiatan="abc", deskripsi="CDF")
        kegiatan.save()
        orang = DaftarOrang(nama_orang="mangoleh",
                      kegiatan=Kegiatan.objects.get(pk=1))
        orang.save()
        response = self.client.get(reverse('story6:delete', args=[orang.pk]))
        self.assertEqual(DaftarOrang.objects.count(), 1)
        self.assertEqual(response.status_code, 302)
        
class TestRegist(TestCase):
    def setUp(self):
        kegiatan = Kegiatan(nama_kegiatan="memotong sayur", deskripsi="membantu mama")
        kegiatan.save()

    def test_regist_POST(self):
        response = Client().post('/story6/register/1/',
            data={'nama_orang': 'Azzahra'})
        self.assertEqual(response.status_code, 404)

    def test_regist_GET(self):
        response = self.client.get('/story6/register/1/')
        self.assertEqual(response.status_code, 404)

    def test_regist_POST_invalid(self):
        response = Client().post('/story6/register/1/',
            data={'nama_orang':''},follow= True )
        self.assertEqual(response.status_code, 404)



class TestUrls(TestCase):
    def setUp(self):
        self.client = Client()

    def apakah_ada_url_index(self):
        response = Client().get('/story6/index/')
        self.assertEquals(response.status_code, 301)

    def apakah_ada_url_tambahKegiatan(self):
        response = Client().get('/story6/tambahKegiatan/')
        self.assertEquals(response.status_code, 301)

    def apakah_ada_url_listKegiatan(self):
        response = Client().get('/story6/listKegiatan/')
        self.assertEquals(response.status_code, 301)

    def apakah_ada_url_register(self):
        response = Client().get('/story6/register/')
        self.assertEquals(response.status_code, 301)


class TestForm(TestCase):
    def form_valid(self):
        form_kegiatan = FormKegiatan(
            data={
                "nama_kegiatan": "memotong sayur",
                "deskripsi": "membantu mama"
            }
        )
        self.assertTrue(form_kegiatan.is_valid())
        form_orang = FormOrang(
            data={
                'nama_orang':"Azzahra"
            }
        )
        self.assertTrue(form_orang.is_valid())

        def form_invalid(self):
            form_kegiatan = FormKegiatan(data={})
            self.assertFalse(form_kegiatan.is_valid())
            form_orang = FormOrang(data={})
            self.assertFalse(form_orang.is_valid())



class TestHTML(TestCase):
    def setUp(self):
        self.client = Client()

    def index_used(self):
        response = Client().get('/story6/index/')
        self.assertTemplateUsed(response, 'story6/index.html')

    def tambahKegiatan_used(self):
        response = Client().get('/story6/tambahKegiatan/')
        self.assertTemplateUsed(response, 'story6/tambahKegiatan.html')
    
    def listKegiatan_used(self):
        response = Client().get('/story6/listKegiatan/')
        self.assertTemplateUsed(response, 'story6/listKegiatan.html')

    def register_used(self):
        response = Client().get('/story6/register/')
        self.assertTemplateUsed(response, 'story6/register.html')

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(Story6Config.name, 'story6')






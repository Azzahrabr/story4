from django.contrib import admin

# Register your models here.
from .models import Kegiatan, DaftarOrang

admin.site.register(Kegiatan)
admin.site.register(DaftarOrang)